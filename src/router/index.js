import Vue from 'vue'
import VueRouter from 'vue-router'
import store, { TYPES } from '../store'
const Home = () => import('../views/Home.vue')
const Login = () => import('../views/Login.vue')
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    meta: {
      requireAuth: true
    }
  },
  {
    path: '/login',
    name: 'login',
    component: Login,
    meta: {
      requireAuth: false
    }
  }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

router.beforeEach((to, from, next) => {
  if (to.meta.requireAuth && !store.getters[TYPES.isAuth]) {
    return next({name: 'login'})
  }
  next()
})
export default router
