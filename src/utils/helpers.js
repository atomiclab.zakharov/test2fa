export function isNumber(n) {
  return !isNaN(+n) && isFinite(n)
}

export const numberAllowedKeys = ['ArrowLeft', 'ArrowRight', 'Backspace']