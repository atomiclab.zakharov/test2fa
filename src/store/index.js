import Vue from 'vue'
import Vuex from 'vuex'
import api from '../api/auth'

Vue.use(Vuex)
export const TYPES = {
  login: 'login',
  token: 'token',
  isAuth: 'isAuth'
}
export default new Vuex.Store({
  state: {
    [TYPES.is2FAOpened]: false,
    [TYPES.token]: ''
  },
  mutations: {
    [TYPES.login](state, token) {
      state[TYPES.token] = token
    }
  },
  getters: {
    [TYPES.isAuth](state) {
      return !!state[TYPES.token]
    }
  },
  actions: {
    async [TYPES.login]({ commit }, { data }) {
      const resObj = !data.code ? {
        status: 401,
        data: {
          '2fa': true
        }
      } : {
        status: 200,
        data: {
          token: 'token'
        }
      }
      const response = await api.login(data, resObj)
      commit(TYPES.login, response.data.token)
    }
  }
})
