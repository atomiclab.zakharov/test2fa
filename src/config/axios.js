export default {
  post (_url, fakeResponse) {
    return new Promise((resolve, reject) => {
      setTimeout(async () => {
        if (fakeResponse.status === 200) {
          return resolve(fakeResponse)
        }
        reject(fakeResponse) 
      }, 1000)
    })
  }
}