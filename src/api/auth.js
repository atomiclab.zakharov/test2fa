import axios from '../config/axios'

export default {
  login (_data, fakeResponse) {
    return axios.post('/login', fakeResponse)
  },
  twoFactorAuth (_code, fakeResponse) {
    return axios.post('/2fa', fakeResponse)
  },
}