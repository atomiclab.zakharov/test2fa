import Vue from 'vue'
import App from './App.vue'
import store from './store'
import router from './router'
import twoFactorAuthentication from './plugins/twoFactorAuthentication'
import './assets/styles/index.scss'
Vue.config.productionTip = false

new Vue({
  store,
  router,
  twoFactorAuthentication,
  render: h => h(App)
}).$mount('#app')
