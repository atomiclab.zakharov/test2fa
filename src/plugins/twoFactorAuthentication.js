import Vue from 'vue'

const Plugin = {}

Plugin.install = (Vue) => {
  Vue.prototype.$2fa = new Vue({
    data () {
      return {
        isOpen: false,
        pendingRequests: [],
        isLoading: false,
        error: null
      }
    },
    methods: {
      setIsOpen (isOpen) {
        this.isOpen = isOpen
      },
      request (fn) {
        const self = this
        return async () => {
          return new Promise((resolve, reject) => {
            const fnWithContext = fn.bind(this)
            const fnRequest = fnWithContext()
            if (fnRequest.then) {
              fnRequest.catch(e => {
                if (e.status === 401) {
                  self.pendingRequests.push({
                    fn: fnWithContext,
                    resolve,
                    reject
                  })
                  return self.isOpen = true
                }
                reject(e)
              })
            } else {
              resolve(fnRequest)
            }
          })
        }
      },
      async submit (code) {
        const promises = this.pendingRequests.map(async (reqOptions) => {
          try {
            const response = await reqOptions.fn(code)
            reqOptions.resolve(response)
          } catch (e) {
            if (e.status === 401) {
              throw e
            }
            reqOptions.reject(e)
          }
        })
        try {
          this.isLoading = true
          await Promise.all(promises)
          this.isOpen = false
        } catch (e) {
          console.warn(e)
          this.error = e
        } finally {
          this.isLoading = false
        }
      }
    }
  })
}

Vue.use(Plugin)

export default Plugin